#!/usr/bin/python3
#
# Performs webfinger lookups on the identifiers in data/acctsOnSite.json
# and generates data/useridsToHref.json

import json
import urllib.parse
import urllib.request

found = {}
failed = {}

acctsOnSiteJson = None
with open('data/acctsOnSite.json', 'r') as fd:
    acctsOnSiteJson = json.load(fd)

for acct in acctsOnSiteJson.keys():
    print( f"{acct} -> ", end="", flush=True )

    userid = '@' + acct[5:] # strip 'acct:' and prefix with @ again
    at = userid.index( '@', 1 )
    domain = userid[at+1:]

    wfUrl = f"https://{domain}/.well-known/webfinger?resource={ urllib.parse.quote(acct) }"
    profileUrl = None
    try:
        wfContent = urllib.request.urlopen(urllib.request.Request(wfUrl, headers={'User-Agent': 'resolve-userids'})).read()
        wfJson = json.loads( wfContent )

        if 'links' in wfJson:
            for linkJson in wfJson['links']:
                if 'rel' in linkJson and linkJson['rel'] == 'http://webfinger.net/rel/profile-page' and 'href' in linkJson:
                    profileUrl = linkJson['href']

    except Exception as e:
        print( f"Failed resolving {acct} with {wfUrl}: {e}" )
        failed[acct] = 1

    if profileUrl :
       found[userid] = profileUrl
    print( profileUrl )

with open('data/useridsToHref.json', 'w') as fd:
    json.dump( found, fd )

with open('data/useridsUnresolveable.json', 'w') as fd:
    json.dump( list(failed.keys()), fd )
