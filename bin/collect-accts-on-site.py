#!/usr/bin/python3
#
# Updates file data/acctsOnSite.json by grep'ing through the content files

import json
import pathlib
import re

fediverseIdPattern = re.compile( "@(([-a-zA-Z_0-9.]+)@([-a-zA-Z0-9.]+))" )
found = {}

mdFiles = pathlib.Path('content').glob('**/*.md')
for mdFile in mdFiles :
    mdFileName = str(mdFile)
    print( f"Processing {mdFileName}..." )

    mdContent = None
    with open(mdFile, 'r') as fd:
        mdContent = fd.read()
    if not mdContent :
        continue

    pos = 0
    while( m := fediverseIdPattern.search( mdContent, pos )) is not None:
        pos = m.end()
        id = 'acct:' + m[1] # not the leading @
        print( f"  Found {id}" )
        if id in found:
            if mdFileName in found[id]:
                found[id][mdFileName] += 1
            else :
                found[id][mdFileName] = 1
        else:
            found[id] = { mdFileName : 1 }

with open('data/acctsOnSite.json', 'w') as fd:
    json.dump( found, fd )
