window.addEventListener( "load", function() {
    var div   = document.getElementById( "countdown" );
    if( div == null ) {
        return;
    }
    var event = div.dataset.event;
    var when  = new Date( div.dataset.when );

    var updateCountdown = function() {
        var now   = new Date().getTime();
        var delta = Math.floor( ( when - now ) / 1000 );

        if( delta > 0 ) {
            var days  = Math.floor( delta / (60 * 60 * 24));
            var hours = Math.floor( delta / (60 * 60)) % 24;
            var mins  = Math.floor( delta / 60 ) % 60;
            var secs  = delta % 60;

            document.getElementById( "countdown-days" ).innerHTML    = days;
            document.getElementById( "countdown-hours" ).innerHTML   = hours <= 9 ? ( "0" + hours ) : hours;
            document.getElementById( "countdown-minutes" ).innerHTML = mins  <= 9 ? ( "0" + mins  ) : mins;
            document.getElementById( "countdown-seconds" ).innerHTML = secs  <= 9 ? ( "0" + secs  ) : secs;

        } else {
            div.innerHTML = "";
            div.style.display = "none";
        }
    };
    div.innerHTML = `
 <div class="row">
  <span><span id="countdown-days">???</span>&nbsp;days</span>
 </div>
 <div class="row">
  <span><span id="countdown-hours">???</span>&nbsp;hours</span>
  <span><span id="countdown-minutes">???</span>&nbsp;minutes</span>
  <span><span id="countdown-seconds">???</span>&nbsp;seconds</span>
 </div>
 <div class="row">
  until
  <span id="countdown-event">${event}</span>
 </div>
`;
    updateCountdown();

    // Update every 1 second
    setInterval( updateCountdown, 1000 );
} );
